import photo1 from './assets/images/1.jpeg';
import photo2 from './assets/images/2.jpeg';
import photo3 from './assets/images/3.jpeg';
import photo4 from './assets/images/4.jpeg';
import photo5 from './assets/images/5.jpeg';
import photo6 from './assets/images/6.jpeg';
import photo7 from './assets/images/7.jpeg';
import photo8 from './assets/images/8.jpeg';

export const data = [
  {
    name: 'InterContinental : Dubai Marina',
    location: 'Dubai',
    capacity: 1,
    address: 'King Salman Bin Abdul Aziz Al Saud Street, Dubai, 393080',
    price: '2000',
    photo: photo1,
  },
  {
    name: 'Crowne Plaza Dubai Marina',
    location: 'Dubai',
    capacity: 1,
    address: 'Al Yahoom Street, Dubai Marina, Dubai',
    price: '3000',
    photo: photo2,
  },
  {
    name: 'Indigo Dubai Downtown',
    location: 'Dubai',
    capacity: 3,
    address: 'Gateway Avenue, Dubai, 410909',
    price: '1800',
    photo: photo3,
  },
  {
    name: 'Grand Millennium Al Wahda Abu Dhabi',
    location: 'Abu Dubai',
    capacity: 3,
    address: 'Gateway Avenue, Dubai, 410909',
    price: '4000',
    photo: photo4,
  },
  {
    name: 'Grand Millennium Al Wahda Abu Dhabi',
    location: 'Abu Dubai',
    capacity: 2,
    address: 'Gateway Avenue, Dubai, 410909',
    price: '4000',
    photo: photo5,
  },
  {
    name: 'Grand Millennium Al Wahda Abu Dhabi',
    location: 'Abu Dubai',
    capacity: 1,
    address: 'Gateway Avenue, Dubai, 410909',
    price: '4000',
    photo: photo6,
  },
  {
    name: 'Grand Millennium Al Wahda Sharjah',
    location: 'Sharjah',
    capacity: 2,
    address: 'Premier Inn Sharjah Capital Centre',
    price: '4000',
    photo: photo7,
  },
  {
    name: 'Novotel Sharjah Al Bustan',
    location: 'Sharjah',
    capacity: 1,
    address: 'Gateway Avenue, Sharjah, 410909',
    price: '4000',
    photo: photo8,
  },
  {
    name: 'InterContinental : Dubai Marina',
    location: 'Dubai',
    capacity: 1,
    address: 'King Salman Bin Abdul Aziz Al Saud Street, Dubai, 393080',
    price: '2000',
    photo: photo1,
  },
  {
    name: 'Crowne Plaza Dubai Marina',
    location: 'Dubai',
    capacity: 2,
    address: 'Al Yahoom Street, Dubai Marina, Dubai',
    price: '3000',
    photo: photo2,
  },
  {
    name: 'Indigo Dubai Downtown',
    location: 'Dubai',
    capacity: 2,
    address: 'Gateway Avenue, Dubai, 410909',
    price: '1800',
    photo: photo3,
  },
  {
    name: 'Grand Millennium Al Wahda Abu Dhabi',
    location: 'Abu Dubai',
    capacity: 3,
    address: 'Gateway Avenue, Dubai, 410909',
    price: '4000',
    photo: photo4,
  },
  {
    name: 'Grand Millennium Al Wahda Abu Dhabi',
    location: 'Abu Dubai',
    capacity: 2,
    address: 'Gateway Avenue, Dubai, 410909',
    price: '4000',
    photo: photo5,
  },
  {
    name: 'Grand Millennium Al Wahda Abu Dhabi',
    location: 'Abu Dubai',
    capacity: 1,
    address: 'Gateway Avenue, Dubai, 410909',
    price: '4000',
    photo: photo6,
  },
  {
    name: 'Grand Millennium Al Wahda Sharjah',
    location: 'Sharjah',
    capacity: 2,
    address: 'Premier Inn Sharjah Capital Centre',
    price: '4000',
    photo: photo7,
  },
  {
    name: 'Novotel Sharjah Al Bustan',
    location: 'Sharjah',
    capacity: 1,
    address: 'Gateway Avenue, Sharjah, 410909',
    price: '4000',
    photo: photo8,
  },
];
