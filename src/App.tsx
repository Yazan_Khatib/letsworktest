import * as React from 'react';
import Datepicker from 'react-datepicker';
import { CheckIcon } from '@heroicons/react/solid';
import { ReactComponent as Marker } from './assets/svg/marker.svg';
import { ReactComponent as Person } from './assets/svg/person.svg';
import { data } from './data';
import { Select, Modal } from './components';
import 'react-datepicker/dist/react-datepicker.css';

const cities = [
  {
    id: 1,
    name: 'Abu Dubai',
  },
  {
    id: 2,
    name: 'Dubai',
  },
  {
    id: 3,
    name: 'Sharjah',
  },
];

const App: React.FC = () => {
  const [city, setCity] = React.useState(cities[2]);
  const [open, setOpen] = React.useState(false);
  const [days, setDays] = React.useState(0);
  const [endDate, setEndDate] = React.useState(new Date());
  const [officies, setOfficies] = React.useState(data);
  const [capacity, setCapacity] = React.useState(1);
  const [startDate, setStartDate] = React.useState(new Date());

  React.useEffect(() => {
    const difference = endDate.getTime() - startDate.getTime();
    const d = Math.ceil(difference / (1000 * 3600 * 24));
    setDays(d);
  }, [startDate, endDate]);

  React.useEffect(() => {
    const result = data.filter((d) => d.location === city.name && d.capacity == capacity);
    setOfficies(result);
  }, [city, capacity]);

  return (
    <>
      <div className='flex flex-col sm:flex-row justify-between items-center border-b px-6 py-4'>
        <img className='h-12' src='./letsworklogo.png' alt='logo' />
        <p>Enjoy workspace freedom with Letswork.</p>
      </div>

      <div className='flex flex-col md:flex-row justify-between border-b p-4'>
        <div className='w-11/12 md:w-1/5 mx-4 mb-4 md:mb-0'>
          <Select city={city} cities={cities} setCity={setCity} />
        </div>

        <div className='w-11/12 md:w-1/5 mb-4 md:mb-0 mx-4'>
          <p className='block text-sm font-medium text-gray-700'>Start Time</p>
          <Datepicker
            className='w-full border rounded-md border-gray-300 shadow-sm cursor-default py-2 px-2 mt-1'
            selected={startDate}
            onChange={(date: any) => setStartDate(date)}
          />
        </div>

        <div className='w-11/12 md:w-1/5 mb-4 md:mb-0 mx-4'>
          <p className='block text-sm font-medium text-gray-700'>End Time</p>
          <Datepicker
            className='w-full border rounded-md border-gray-300 shadow-sm cursor-default py-2 px-2 mt-1'
            selected={endDate}
            onChange={(date: any) => setEndDate(date)}
          />
        </div>

        <div className='w-11/12 md:w-1/5 mb-4 md:mb-0 mx-4'>
          <p className='block text-sm font-medium text-gray-700'>Capacity</p>
          <input
            type='number'
            value={capacity}
            onChange={(e: any) => setCapacity(e.target.value)}
            className='w-full border rounded-md border-gray-300 shadow-sm cursor-default py-2 px-2 mt-1'
          />
        </div>
      </div>

      <div className='m-8'>
        <div className='font-semibold text-gray-700'>
          {officies.length} {officies.length > 1 ? 'results' : 'result'} matching your search
          entries
        </div>
        <div className='flex flex-wrap justify-between'>
          {officies.map((office) => {
            return (
              <div className='w-full xl:w-card flex flex-col md:flex-row justify-between border rounded-lg my-4 mr-2 p-6'>
                <Modal
                  open={open}
                  setOpen={setOpen}
                  days={days}
                  city={city.name}
                  end={endDate}
                  start={startDate}
                  price={office.price}
                />

                <img
                  className='w-full md:h-48 md:w-72 rounded-md mr-4'
                  src={office.photo}
                  alt='Office space'
                />
                <div>
                  <p className='text-center text-lg font-semibold underline'>{office.name}</p>
                  <div className='flex justify-center items-start'>
                    <Marker className='w-10 mr-2' />
                    <p className='text-sm font-normal underline'>{office.address}</p>
                  </div>

                  <div className='flex justify-center items-center mt-2'>
                    <Person className='w-7 mr-2' />
                    <p className='text-sm font-normal'>{office.capacity}</p>
                  </div>
                </div>

                <div className='flex flex-col justify-between'>
                  <p className='text-gray-600 text-center'>AED {office.price}/day</p>
                  <div>
                    <div className='flex text-xs text-green-700'>
                      <CheckIcon className='h-5 w-1/5' /> <p>Book instantly</p>
                    </div>
                    <div className='flex text-xs text-green-700'>
                      <CheckIcon className='h-5 w-1/5' />
                      <p>Cancel for free up to 24 hours before</p>
                    </div>
                  </div>
                  <button
                    onClick={() => setOpen(true)}
                    className='w-full bg-blue-600 text-white rounded-3xl py-1 px-2 mt-2'>
                    Book now
                  </button>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </>
  );
};

export default App;
